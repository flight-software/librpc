#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "libdebug.h"
#include "libsll.h"
#include "libudp.h"
#include "librpc.h"

typedef int64_t ACK_TYPE_NEW;

static int rpc_op_get_pid(const void* in, size_t in_len, void* out, size_t* out_len, void* param)
{
    (void)in;
    (void)in_len;
    (void)param;

    if(*out_len < sizeof(pid_t)) {
        P_ERR("out len smaller than pid, %zu < %zu", *out_len, sizeof(pid_t));
        return EXIT_FAILURE;
    }

    *((pid_t*)out) = getpid();
    *out_len = sizeof(pid_t);

    return EXIT_SUCCESS;
}

static void* rpc_server_loop_mt(rpc_server_fs* rpc_server)
{
    while(rpc_server->running) {
        rpc_server_loop(rpc_server);
        usleep(10000);
    }
    return NULL;
}

int rpc_server_init(rpc_server_fs* rpc_server, uint16_t port, int flags)
{
    if(!rpc_server) {
        P_ERR_STR("server is null");
        return EXIT_FAILURE;
    }

    memset(rpc_server, 0, sizeof(rpc_server_fs));
    
    if(sll_init(&rpc_server->elem_list, NULL)) {
        P_ERR_STR("Error in initiation of list");
        return EXIT_FAILURE;
    }


    if(flags & RPC_SERVER_MT) {
        // Use blocking socket in MT mode to not burn CPU
        if(setup_udp_socket(&rpc_server->udp_fd, port)) {
            sll_close(&rpc_server->elem_list);
            return EXIT_FAILURE;
        }
    } else {
        if(setup_udp_socket_NB(&rpc_server->udp_fd, port)) {
            sll_close(&rpc_server->elem_list);
            return EXIT_FAILURE;
        }
    }

    // Add RPC OP
    rpc_server_add_elem(rpc_server, RPC_OP_GET_PID, rpc_op_get_pid, NULL, NULL);

    rpc_server->flags = flags;
    rpc_server->port = port;
    rpc_server->running = 1;

    if(flags & RPC_SERVER_MT) {
        pthread_mutex_init(&rpc_server->lock, NULL);
        if(pthread_create(&rpc_server->thread, NULL, (void*(*)(void*))rpc_server_loop_mt, rpc_server)) {
            rpc_server->flags &= ~(RPC_SERVER_MT);
            P_ERR("failed to create thread for server, errno: %d (%s)", errno, strerror(errno));
            close(rpc_server->udp_fd);
            sll_close(&rpc_server->elem_list);
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

int rpc_server_add_elem(rpc_server_fs* rpc_server, uint16_t id, rpc_cb elem, void* param, pthread_mutex_t* mutex)
{
    if(!rpc_server) {
        P_ERR_STR("server is null");
        return EXIT_FAILURE;
    }

    if(!elem) {
        P_ERR_STR("callback is null");
        return EXIT_FAILURE;
    }

    if(rpc_server->flags & RPC_SERVER_MT) {
        pthread_mutex_lock(&rpc_server->lock);
    }

    rpc_elem_fs* rpc_elem = NULL;
    SLL_ITER(rpc_server->elem_list, rpc_elem_fs, cur_elem) {
        if(cur_elem->id == id){
            rpc_elem = cur_elem;
            break;
        }
    }

    if(!rpc_elem) {
        rpc_elem = calloc(sizeof(rpc_elem_fs), 1);
        if(!rpc_elem) {
            P_ERR("calloc failed, errno: %d (%s)", errno, strerror(errno));
            goto rpc_server_add_elem_fail;
        }
        rpc_elem->id = id;
        rpc_elem->elem = elem;
        rpc_elem->param = param;
        rpc_elem->mutex = mutex;
        if(sll_push_head(&rpc_server->elem_list, rpc_elem, sizeof(rpc_elem_fs), SLL_NOWRITE)) {
            P_ERR_STR("failed to push elem to sll");
            free(rpc_elem);
            goto rpc_server_add_elem_fail;
        }
    }

    if(rpc_server->flags & RPC_SERVER_MT) {
        pthread_mutex_unlock(&(rpc_server->lock));
    }

    return EXIT_SUCCESS;

rpc_server_add_elem_fail:
    if(rpc_server->flags & RPC_SERVER_MT){
        pthread_mutex_unlock(&rpc_server->lock);
    }
    
    return EXIT_FAILURE;
}

static int rpc_server_loop_run_elem(const rpc_server_fs* rpc_server, const rpc_elem_fs* rpc_elem, const void* in, size_t in_size, void* out, size_t* out_size)
{
    if(!rpc_server) {
        P_ERR_STR("server is null");
        return EXIT_FAILURE;
    }

    if(!rpc_elem) {
        P_ERR_STR("rpc elem is null");
        return EXIT_FAILURE;
    }

    if(!out) {
        P_ERR_STR("out is null");
        return EXIT_FAILURE;
    }

    if(!out_size) {
        P_ERR_STR("out size is null");
        return EXIT_FAILURE;
    }

    if(*out_size < sizeof(ACK_TYPE_NEW)) {
        P_ERR("out size is too small %zu < %zu", *out_size, sizeof(ACK_TYPE_NEW));
        return EXIT_FAILURE;
    }

    const uint8_t elem_mutex = !!(rpc_server->flags & RPC_SERVER_MT) && rpc_elem->mutex;

    const void* cb_in = (const uint8_t*)in + sizeof(uint16_t);
    size_t cb_in_size = in_size - sizeof(uint16_t);

    void* cb_out = (uint8_t*)out + sizeof(ACK_TYPE_NEW);
    size_t cb_out_size = (*out_size) - sizeof(ACK_TYPE_NEW);

    if(elem_mutex) {
        pthread_mutex_lock(rpc_elem->mutex);
        
        ACK_TYPE_NEW ret = rpc_elem->elem(cb_in, cb_in_size, cb_out, &cb_out_size, rpc_elem->param);
        
        memcpy(out, &ret, sizeof(ACK_TYPE_NEW));
        
        pthread_mutex_unlock(rpc_elem->mutex);
    } else {
        ACK_TYPE_NEW ret = rpc_elem->elem(cb_in, cb_in_size, cb_out, &cb_out_size, rpc_elem->param);
        
        memcpy(out, &ret, sizeof(ACK_TYPE_NEW));
    }

    *out_size = cb_out_size + sizeof(ACK_TYPE_NEW);

    return EXIT_SUCCESS;
}

int rpc_server_loop(rpc_server_fs* rpc_server)
{
    if(!rpc_server) {
        P_ERR_STR("server is null");
        return EXIT_FAILURE;
    }

    static uint8_t in[8192];
    static uint8_t out[8192];

    size_t in_size = sizeof(in);
    size_t out_size = sizeof(out);

    struct sockaddr_in src_addr;
    socklen_t src_addr_len = sizeof(src_addr);

    ssize_t read_ret = recvfrom(rpc_server->udp_fd, in, in_size, 0, (struct sockaddr *)&src_addr, &src_addr_len);

    if(read_ret < 0) {
        if(errno == EAGAIN
#if EAGAIN != EWOULDBLOCK
        || errno == EWOULDBLOCK
#endif
          ) {
            goto rpc_server_success;
        }
        P_ERR("recvfrom failed, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    if(read_ret == 0) {
        P_ERR_STR("recvfrom returned 0, otherside shutdown?");
        return EXIT_FAILURE;
    }

    const uint16_t op = *((uint16_t*)&(in[0]));
    in_size = (size_t)read_ret;

    if(rpc_server->flags & RPC_SERVER_MT) {
        pthread_mutex_lock(&rpc_server->lock);
    }

    SLL_ITER(rpc_server->elem_list, rpc_elem_fs, cur_elem) {
        if(cur_elem->id == op) {
            P_DEBUG("Running op code: %d", op);
            if(rpc_server_loop_run_elem(rpc_server, cur_elem, in, in_size, out, &out_size)) {
                P_ERR("failed to run op: %"PRIu16, op);
                goto rpc_server_fail;
            }

            ssize_t sent = sendto(rpc_server->udp_fd, out, out_size, 0, (struct sockaddr*)&src_addr, src_addr_len);
            
            if(sent < 0) {
                P_ERR("sendto failed, errno: %d (%s)", errno, strerror(errno));
                goto rpc_server_fail;
            }

            if((size_t)sent != out_size) {
                P_ERR("sendto did not send all of the bytes, %zu < %zu", (size_t)sent, out_size);
                goto rpc_server_fail;
            }

            goto rpc_server_success;
        }
    }

    P_ERR("Unmatched op code: %"PRIu16, op);

    memcpy(out, &(ACK_TYPE_NEW){EXIT_FAILURE}, sizeof(ACK_TYPE_NEW));
    
    sendto(rpc_server->udp_fd, out, out_size, 0,  (struct sockaddr*)&src_addr, src_addr_len);
    
    goto rpc_server_fail;

rpc_server_fail:
    if(rpc_server->flags & RPC_SERVER_MT) {
        pthread_mutex_unlock(&(rpc_server->lock));
    }
    return EXIT_FAILURE;

rpc_server_success:
    if(rpc_server->flags & RPC_SERVER_MT) {
        pthread_mutex_unlock(&(rpc_server->lock));
    }
    return EXIT_SUCCESS;
}

static int rpc_send_recv_check_ack(const void* data, size_t data_len)
{

    if(data_len < sizeof(ACK_TYPE_NEW)) {
        P_ERR("only received %zu bytes as response", data_len);
        return EXIT_FAILURE;
    }
    
    ACK_TYPE_NEW ack;
    memcpy(&ack, data, sizeof(ACK_TYPE_NEW));

    if(ack == EXIT_FAILURE) {
        P_ERR_STR("ack response was fail");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

static int rpc_send_recv_no_in_out(uint16_t port, uint16_t op, struct timeval* tv, void* data, size_t data_len)
{
    memcpy(data, &op, sizeof(op));

    if(send_msg_and_recv_response(port, data, sizeof(op), data, &data_len, tv)) {
        P_ERR_STR("failed to send msg and recv response");
        return EXIT_FAILURE;
    }

    return rpc_send_recv_check_ack(data, data_len);
}

static int rpc_send_recv_no_out(uint16_t port, uint16_t op, const void* in, size_t in_len, struct timeval* tv, void* data, size_t data_len)
{
    {
        void* p = mempcpy(data, &op, sizeof(op));
        memcpy(p, in, in_len);
    }

    if(send_msg_and_recv_response(port, data, sizeof(op) + in_len, data, &data_len, tv)) {
        P_ERR_STR("failed to send msg and recv response");
        return EXIT_FAILURE;
    }

    return rpc_send_recv_check_ack(data, data_len);
}

static int rpc_send_recv_check_copy_out(void* out, size_t* out_len, const void* data, size_t data_len)
{
    data_len -= sizeof(ACK_TYPE_NEW);
    data = (const uint8_t*)data + sizeof(ACK_TYPE_NEW);

    if(*out_len < data_len) {
        P_ERR("got %zu bytes but only expected %zu bytes", data_len, *out_len);
        return EXIT_FAILURE;
    }

    memcpy(out, data, data_len);
    
    *out_len = data_len;

    return EXIT_SUCCESS;
}

static int rpc_send_recv_no_in(uint16_t port, uint16_t op, void* out, size_t* out_len, struct timeval* tv, void* data, size_t data_len)
{
    memcpy(data, &op, sizeof(op));

    if(send_msg_and_recv_response(port, data, sizeof(op), data, &data_len, tv)) {
        P_ERR_STR("failed to send msg and recv response");
        return EXIT_FAILURE;
    }

    if(rpc_send_recv_check_ack(data, data_len)) return EXIT_FAILURE;

    return rpc_send_recv_check_copy_out(out, out_len, data, data_len);
}

static int rpc_send_recv_in_out(uint16_t port, uint16_t op, const void* in, size_t in_len, void* out, size_t* out_len, struct timeval* tv, void* data, size_t data_len)
{
    {
        void* p = mempcpy(data, &op, sizeof(op));
        memcpy(p, in, in_len);
    }

    if(send_msg_and_recv_response(port, data, sizeof(op) + in_len, data, &data_len, tv)) {
        P_ERR_STR("failed to send msg and recv response");
        return EXIT_FAILURE;
    }

    if(rpc_send_recv_check_ack(data, data_len)) return EXIT_FAILURE;

    return rpc_send_recv_check_copy_out(out, out_len, data, data_len);
}

// No relation to rpc_server_fs, so doesn't have lock nonsense
int rpc_send_recv(uint16_t port, uint16_t op, const void* in, size_t in_len, void* out, size_t* out_len, uint64_t timeout_micro_s)
{
    static uint8_t data[8192];

    int_fast8_t do_in = in ? 1 : 0;
    int_fast8_t do_out = out && out_len ? 1 : 0;

    if(do_in) {
        if(in_len > (sizeof(data) - sizeof(op))) {
            P_ERR("in len %zu too big", in_len);
            return EXIT_FAILURE;
        }
        if(in_len == 0) {
            P_ERR_STR("in len should not be 0");
            return EXIT_FAILURE;
        }
    }

    if(do_out) {
        if(*out_len > (sizeof(data) - sizeof(ACK_TYPE_NEW))) {
            P_ERR("out len %zu too big", *out_len);
            return EXIT_FAILURE;
        }
        if(*out_len == 0) {
            P_ERR_STR("out len should not be 0");
            return EXIT_FAILURE;
        }
    }

    struct timeval tv = {
        .tv_sec = (time_t)(timeout_micro_s / UINT64_C(1000000)),
        .tv_usec = (suseconds_t)(timeout_micro_s % UINT64_C(1000000))
    };

    if(do_in && do_out) {
        return rpc_send_recv_in_out(port, op, in, in_len, out, out_len, &tv, data, sizeof(data));
    }

    if(do_out) {
        return rpc_send_recv_no_in(port, op, out, out_len, &tv, data, sizeof(data));
    }

    if(do_in) {
        return rpc_send_recv_no_out(port, op, in, in_len, &tv, data, sizeof(data));
    }

    return rpc_send_recv_no_in_out(port, op, &tv, data, sizeof(data));
}

int rpc_trigger_action(uint16_t port, uint16_t op, uint64_t timeout_micro_s)
{
    return rpc_send_recv(port, op, NULL, 0, NULL, NULL, timeout_micro_s);
}

// Buffer rpc calls
int rpc_send_buf(uint16_t port, uint16_t op, const void* in, size_t in_size, uint64_t timeout_micro_s)
{
    return rpc_send_recv(port, op, in, in_size, NULL, NULL, timeout_micro_s);
}

int rpc_recv_buf(uint16_t port, uint16_t op, void* out, size_t out_size, uint64_t timeout_micro_s)
{
    const size_t orig_out_size = out_size;
    if(rpc_send_recv(port, op, NULL, 0, out, &out_size, timeout_micro_s)) {
        return EXIT_FAILURE;
    }
    if(out_size != orig_out_size) {
        P_ERR("expected %zu bytes got %zu bytes", orig_out_size, out_size);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int rpc_recv_buf_varlen(uint16_t port, uint16_t op, void* out, size_t* out_size, uint64_t timeout_micro_s)
{
    return rpc_send_recv(port, op, NULL, 0, out, out_size, timeout_micro_s);
}


int rpc_server_close(rpc_server_fs* rpc_server)
{
    if(!rpc_server) {
        P_ERR_STR("server is null");
        return EXIT_FAILURE;
    }
    
    // Don't need to lock
    rpc_server->running = 0;
    
    int ret = EXIT_SUCCESS;

    if(pthread_join(rpc_server->thread, NULL)) {
        P_ERR("pthread join failed, errno: %d (%s)", errno, strerror(errno));
        ret = EXIT_FAILURE;
    }
    
    if(pthread_mutex_destroy(&rpc_server->lock)) {
        P_ERR("pthread mutex destroy failed, errno: %d (%s)", errno, strerror(errno));
        ret = EXIT_FAILURE;
    }

    if(sll_close(&rpc_server->elem_list)) {
        P_ERR_STR("sll close failed");
        ret = EXIT_FAILURE;
    }

    close(rpc_server->udp_fd);
    
    rpc_server->udp_fd = -1;
    
    return ret;
}

void rpc_format_buffer(uint16_t op, const void* in, size_t in_len, void** buffer, size_t* buffer_len)
{
    *buffer_len = sizeof(op) + in_len;
    *buffer = malloc(*buffer_len);
    {
        void* p = mempcpy(*buffer, &op, sizeof(op));
        memcpy(p, in, in_len);
    }
}

void rpc_parse_buffer(const void* in, size_t in_len, int* success, void** buffer, size_t* buffer_len)
{
    ACK_TYPE_NEW ack;
    memcpy(&ack, in, sizeof(ACK_TYPE_NEW));

    if(ack == EXIT_FAILURE) {
        *success = EXIT_FAILURE;
        if(in_len <= sizeof(ACK_TYPE_NEW)) {
            *buffer_len = 0;
            *buffer = NULL;
            return;
        }
    }

    *buffer_len = in_len - sizeof(ACK_TYPE_NEW);
    *buffer = malloc(*buffer_len);
    
    memcpy(*buffer, (const uint8_t*)in + sizeof(ACK_TYPE_NEW), *buffer_len);
}

// callback

#define RPC_GET_SET_DEF(type__)                                                                           \
int rpc_set_ ## type__ (const void* in, size_t in_len, void* out, size_t* out_len, void* param)           \
{                                                                                                         \
    (void)out;                                                                                            \
    *out_len = 0;                                                                                         \
    if(in_len != sizeof(type__ ## _t)) {                                                                  \
        P_ERR("in len should be %zu bytes got %zu bytes", sizeof(type__ ## _t), in_len);                  \
        return EXIT_FAILURE;                                                                              \
    }                                                                                                     \
    memcpy(param, in, sizeof(type__ ## _t));                                                              \
    return EXIT_SUCCESS;                                                                                  \
}                                                                                                         \
                                                                                                          \
int rpc_get_ ## type__ (const void* in, size_t in_len, void* out, size_t* out_len, void* param)           \
{                                                                                                         \
    (void)in; (void)in_len;                                                                               \
    if(*out_len != sizeof(type__ ## _t)) {                                                                \
        P_ERR("out len should be %zu bytes got %zu bytes", sizeof(type__ ## _t), out_len);                \
        return EXIT_FAILURE;                                                                              \
    }                                                                                                     \
    memcpy(out, param, sizeof(type__ ## _t));                                                             \
    return EXIT_SUCCESS;                                                                                  \
}                                                                                                         \
                                                                                                          \
int rpc_send_ ## type__ (uint16_t port, uint16_t op, type__ ## _t var, uint64_t timeout_micro_s)          \
{                                                                                                         \
    return rpc_send_recv(port, op, &var, sizeof(var), NULL, NULL, timeout_micro_s);                       \
}                                                                                                         \
                                                                                                          \
int rpc_recv_ ## type__ (uint16_t port, uint16_t op, type__ ## _t* var, uint64_t timeout_micro_s)         \
{                                                                                                         \
    size_t out_len = sizeof(*var);                                                                        \
    if(rpc_send_recv(port, op, NULL, 0, var, &out_len, timeout_micro_s)) {                                \
        return EXIT_FAILURE;                                                                              \
    }                                                                                                     \
    if(out_len != sizeof(*var)) {                                                                         \
        P_ERR("out len should be %zu bytes got %zu bytes", sizeof(*var), out_len);                        \
        return EXIT_FAILURE;                                                                              \
    }                                                                                                     \
    return EXIT_SUCCESS;                                                                                  \
}                                                                                                         \
                                                                                                          \
void rpc_format_buffer_ ## type__ (uint16_t op, type__ ## _t var, void** buffer, size_t* buffer_len)      \
{                                                                                                         \
    rpc_format_buffer(op, &var, sizeof(var), buffer, buffer_len);                                         \
}                                                                                                         \
                                                                                                          \
void rpc_parse_buffer_ ## type__ (const void* buffer, int* success, type__ ## _t* var)                    \
{                                                                                                         \
    size_t in_len = sizeof(ACK_TYPE_NEW) + sizeof(*var);                                                  \
    type__ ## _t* out;                                                                                    \
    size_t out_len;                                                                                       \
    rpc_parse_buffer(buffer, in_len, success, (void**)&out, &out_len);                                    \
    *var = *out;                                                                                          \
    free(out);                                                                                            \
}

RPC_GET_SET_DEF(uint64)
RPC_GET_SET_DEF(int64)

RPC_GET_SET_DEF(uint32)
RPC_GET_SET_DEF(int32)

RPC_GET_SET_DEF(uint16)
RPC_GET_SET_DEF(int16)

RPC_GET_SET_DEF(uint8)
RPC_GET_SET_DEF(int8)

RPC_GET_SET_DEF(double)
RPC_GET_SET_DEF(long_double)

#undef RPC_GET_SET_DEF

