#ifndef LIBRPC_H
#define LIBRPC_H

#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>

#include "libsll.h"

/**
 * \file librpc.h
 * \brief UDP RPC as a library
 *
 * Server Side:
 * We define a two byte OP code, and a callback to pass the rest of the
 * buffer as well. rpc_server_loop() calls the callback with input and
 * output buffers and sizes when data is received. There is an ACK_TYPE
 * passed internally (technically ACK_TYPE_NEW so it's 8-byte aligned),
 * and the return value of the function determines whether it's an ACK
 * or a NACK
 *
 * Callbacks can have parameters passed to them, which msot of the time
 * can be NULL, but ADCS can use this to select one specific piece of
 * hardware, and all can easily be addressed with some GET/SET op code
 * ORd with some flags to denote the piece of hardware. For now just
 * use NULL
 *
 * The input data is guaranteed to be 8-byte aligned, so you can treat
 * it as any type without any UB (powerboard just writes an array of
 * calibrated doubles for everything)
 *
 * If what you want to access is blocked by a mutex, you can specify
 * that as the final parameter to rpc_server_add_elem, and the callback
 * can safely assume the mutex has been acquired (mostly used in
 * conjunction with exposing individual variables, if you have more
 * complicated operations it might make more sense to use NULL here
 * and acquire manually in the callback)
 *
 * Client Side:
 * We create a new UDP port with rpc_send_recv (any other functions are
 * simplifications of this), send the data to the daemon, wait for a
 * response, and return EXIT_SUCCESS or EXIT_FAILURE based on this
 */

// Universal RPC commands
#define RPC_OP_GET_PID UINT16_C(UINT16_MAX)
#define RPC_OP_SPLIT_LOG UINT16_C(UINT16_MAX - 1) // universal but each daemon must explicitly implement it

typedef int (*rpc_cb)(const void*, size_t, void*, size_t*, void*);

struct rpc_elem_fs {
    rpc_cb elem;
    void* param;
    pthread_mutex_t* mutex;
    uint16_t id;
};

typedef struct rpc_elem_fs rpc_elem_fs;

struct rpc_server_fs {
    sll_fs elem_list;
    pthread_mutex_t lock;
    pthread_t thread;
    int udp_fd;
    int flags;
    uint16_t port;
    uint8_t running;
};

typedef struct rpc_server_fs rpc_server_fs;

// Run and fill RPC requests in another threaqd
// If this is set, make sure everything has mutual exclusion
// It is NOT a requirement that every OP code have a bound mutex,
// but it IS a requirement that every binding to a native type has one

#define RPC_SERVER_MT 1

int rpc_server_init(rpc_server_fs* rpc_server, uint16_t port, int flags);

int rpc_server_add_elem(rpc_server_fs* rpc_server, uint16_t id, rpc_cb elem, void *param, pthread_mutex_t *mutex);

int rpc_server_loop(rpc_server_fs* rpc_server);

int rpc_server_close(rpc_server_fs* rpc_server);

int rpc_send_recv(uint16_t port, uint16_t op, const void* in, size_t in_len, void* out, size_t* out_len, uint64_t timeout_micro_s);

int rpc_trigger_action(uint16_t port, uint16_t op, uint64_t timeout_micro_s);

int rpc_send_buf(uint16_t port, uint16_t op, const void* in, size_t in_size, uint64_t timeout_micro_s);
int rpc_recv_buf(uint16_t port, uint16_t op, void* out, size_t out_size, uint64_t timeout_micro_s);

int rpc_recv_buf_varlen(uint16_t port, uint16_t op, void* out, size_t* out_size, uint64_t timeout_micro_s);

/**
 * For format and parse buffer, caller is responsible for freeing output pointers `buffer` and `out`
 */
void rpc_format_buffer(uint16_t op, const void* in, size_t in_len, void** buffer, size_t* buffer_len);
void rpc_parse_buffer(const void* buffer, size_t buffer_len, int* success, void** out, size_t* out_len);

// Useful generic callbacks that use the parameter field, mostly for generic getters and
// setters
typedef long double long_double_t;
typedef double double_t; // Autoexpands, just works tho

// Instead of defining getting and setting arbitrary buffers, we directly
// tie remote calls to local getters and setters, so we can be more strict
// about input sizes and generally reduce UB and be more correct
// For rpc_parse_buffer_type, it assumes the buffer is the correct length
// so only use it on buffers returned successfully by librpc
#define RPC_GET_SET_DEC(type__)                                                                       \
int rpc_set_ ## type__ (const void* in, size_t in_len, void* out, size_t* out_len, void* param);      \
int rpc_get_ ## type__ (const void* in, size_t in_len, void* out, size_t* out_len, void* param);      \
int rpc_send_ ## type__ (uint16_t port, uint16_t op, type__ ## _t var, uint64_t timeout_micro_s);     \
int rpc_recv_ ## type__ (uint16_t port, uint16_t op, type__ ## _t* var, uint64_t timeout_micro_s);    \
void rpc_format_buffer_ ## type__ (uint16_t op, type__ ## _t var, void** buffer, size_t* buffer_len); \
void rpc_parse_buffer_ ## type__ (const void* buffer, int* success, type__ ## _t* var);

RPC_GET_SET_DEC(uint64)
RPC_GET_SET_DEC(int64)

RPC_GET_SET_DEC(uint32)
RPC_GET_SET_DEC(int32)

RPC_GET_SET_DEC(uint16)
RPC_GET_SET_DEC(int16)

RPC_GET_SET_DEC(uint8)
RPC_GET_SET_DEC(int8)

RPC_GET_SET_DEC(double)
RPC_GET_SET_DEC(long_double)

#undef RPC_GET_SET_DEC

#endif
